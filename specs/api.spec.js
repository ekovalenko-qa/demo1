import { apiProvider } from '../framework';

test('Provider demo', async () => {

    const body = {
        username: 'demo',
        password: 'demo',
    };
    const r = await apiProvider().login().post(body);
    expect(r.status).toBe(200);
});
